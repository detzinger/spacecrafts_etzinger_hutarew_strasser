#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"

#include "LuaScriptManager.h"

using namespace Ogre;

class PatrolState : public State{

public: 
	PatrolState(AIController* controller, const Path& path):
		State(controller),
		mPath(path),
		mCurrentParam(0)
	{}

	~PatrolState(){}

	virtual void update(float delta){
		Vector3 linear = mController->followPath(mPath, mCurrentParam);
		mController->getSpacecraft()->setSteeringCommand(linear);
	}

	virtual void enter(){
		mCurrentParam = 0;
	}

 

private:
	Path mPath;
	float mCurrentParam;
};


AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrolPath):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mCurrentParam(0.0f),
	mPath(patrolPath)
{
	/*PatrolState* patrol = new PatrolState(this, patrolPath);
	fsm = new StateMachine();
	fsm->addState(patrol);
	fsm->setCurrentState(patrol);
*/
	//scripting::Manager::getSingleton().callFunction<void*>("init", this);

	root = scripting::Manager::getSingleton().callFunction<luabind::object>("init", this);

	// create a overlay to render the behavior tree
	DebugOverlay::getSingleton().addTextBox("ai" + spacecraft->getId(), "", 0, 100 + (250 * (spacecraft->getId() - 1)), 300, 150);
}

void AIController::update(float delta)
{
	//fsm->update(delta);

	if (root)
	{
		root["tick"](root, delta);

		// "renders" the behavior tree on the overlay
		std::string text = luabind::object_cast<std::string>(root["printTree"](root, getSpacecraft()->getId()));
		DebugOverlay::getSingleton().setText("ai" + getSpacecraft()->getId(), text);
	}

	//scripting::Manager::getSingleton().callFunction("update", this);

	//Vector3 linearSteering = followPath(mPath, mCurrentParam);

	//getSpacecraft()->setSteeringCommand(linearSteering);
}


 
