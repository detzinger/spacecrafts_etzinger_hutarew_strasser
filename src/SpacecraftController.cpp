#include "StdAfx.h"

#include "SpacecraftController.h"
#include "DebugDisplay.h"
#include "GameConfig.h"
#include "Path.h"
#include "MathUtil.h"
#include "WorldUtil.h"

using namespace Ogre;

static ColourValue DEBUG_COLOUR_WALL_AVOIDANCE(1.0f, 0.0f, 0.0f);
static ColourValue DEBUG_COLOUR_OBSTALCE_AVOIDANCE(1.0f, 0.0f, 1.0f);


SpacecraftController::SpacecraftController(Spacecraft* spacecraft):
	mSpacecraft(spacecraft)
{}


SpacecraftController::~SpacecraftController()
{}

Ogre::Vector3 SpacecraftController::seek(const Ogre::Vector3& target) const
{
	Ogre::Vector3 desiredVelocity = target - mSpacecraft->getPosition();
	desiredVelocity.normalise();
	desiredVelocity *= Spacecraft::MAX_SPEED;

	return desiredVelocity - mSpacecraft->getLinearVelocity();
}

Ogre::Vector3 SpacecraftController::pursue(Spacecraft& evader) const
{
	
	Ogre::Vector3 toEvader = evader.getPosition() - mSpacecraft->getPosition();
	float  distanceToEvader = toEvader.length();
	//  calculate  lookahead  time  based  on  distance  and  speeds
	float  lookAheadTime = distanceToEvader / (mSpacecraft->MAX_SPEED + evader.getAngularVelocity());
	//  seek  predicted  position
	return  seek(evader.getPosition() + (evader.getAngularVelocity()  *  lookAheadTime));
		
}

Ogre::Vector3 SpacecraftController::arrive(const Ogre::Vector3& target) const
{
	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 16, ColourValue::Red);

	Ogre::Vector3 toTarget = target - mSpacecraft->getPosition();
	float dist = toTarget.length();

	if (dist > 0)
	{
		const float DECELERATION_TWEAKER = GameConfig::getSingleton().getValueAsReal("Steering/DecelerationTweaker");
		float speed = dist / DECELERATION_TWEAKER;

		speed = std::min(speed, Spacecraft::MAX_SPEED);
		Ogre::Vector3 desiredVelocity = toTarget * speed / dist;

		return desiredVelocity - mSpacecraft->getLinearVelocity();
	}

	return Ogre::Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::followPath(const Path& path, float& currentParam) const
{
	if (path.isEmpty())
	{
		return Vector3(0.0f);
	}

	// durch den 2. parameter bei getParam wir ddie Suche beschleunigt
	currentParam = path.getParam(mSpacecraft->getPosition(), currentParam);

	//20 ist der offset, den man sich bewegt von dem punkt weg zu einem anderen hin
	float targetParam = currentParam + GameConfig::getSingleton().getValueAsReal("Steering/PathFollowingTargetOffset");

	//gibt die eigene Position und die Position des Zieles zur�ck
	Vector3 currentPosition = path.getPosition(currentParam);
	Vector3 targetPosition = path.getPosition(targetParam);

	//Am besten immer DebugDraw oder DebugDisplay oder so verwenden!

	if (path.isPathEnd(targetParam))
	{
		return arrive(targetPosition);
	}

	return seek(targetPosition);
}

Ogre::Vector3 SpacecraftController::wallAvoidance(const Spacecraft* spacecraft) const
{
	float lookAheadTime = GameConfig::getSingleton().getValueAsReal("Steering/LookAheadTime");
	float avoidDistance = GameConfig::getSingleton().getValueAsReal("Steering/AvoidDistance");

	Vector3 rayVector = spacecraft->getLinearVelocity();
	rayVector *= lookAheadTime;

	Vector3 colPoint;
	Vector3 colNormal;

	if (WorldUtil::rayCast(spacecraft->getPosition(), spacecraft->getPosition() + rayVector, colPoint, colNormal))
	{
		//if collision occurs calculate seek target  
		Vector3 target = colPoint + colNormal * avoidDistance;

		return seek(target);
	}

	return Ogre::Vector3(0.0f);
}


Ogre::Vector3 SpacecraftController::obstacleAvoidance() const
{
	//durch das const k�nnen Optimierungen gemacht werden bzw wird gew�hrleistet dass alle elemente des
	//Vectors auch const sind!
	const std::vector<Spacecraft*> spacecrafts = WorldUtil::getAllSpacecrafts();

	//for each spacecraft pointer im vector spacecrafts ...
	for (const Spacecraft* obstacle : spacecrafts)
	{
		if (obstacle == mSpacecraft)
		{
			continue;
		}

		float t0, t1;

		if (MathUtil::sphereSweepTest(mSpacecraft->getRadius(), mSpacecraft->getPosition(), mSpacecraft->getLinearVelocity(),
			obstacle->getRadius(), obstacle->getPosition(), obstacle->getLinearVelocity(), t0, t1))
		{
			//zB alles f�r 5 Sekunden ist mir egal
			if (t0 >= 0.0f && t0 < 5.0f)
			{
				//wo die beiden sein werden
				Vector3 myPos = mSpacecraft->getPosition() + mSpacecraft->getLinearVelocity() * t0;
				Vector3 obstaclePos = obstacle->getPosition() + obstacle->getLinearVelocity() * t0;

				Vector3 dir = obstaclePos - myPos;
				dir.normalise();

				return -dir * Spacecraft::MAX_SPEED;
			}
		}
	}

	return Ogre::Vector3(0.0f);
}