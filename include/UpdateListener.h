#pragma once

#include "Ogre.h"
#include "OIS.h"
#include "OgreFrameListener.h"

#include "FileWatcher\FileWatcher.h"
#include "FileWatcher\FileWatcherWin32.h"

namespace FW
{

	// where UpdateListener is defined as such
	class UpdateListener : public FW::FileWatchListener
	{
	public:
		UpdateListener() {}

		void handleFileAction(FW::WatchID watchid, const String& dir, const String& filename,
			FW::Action action)
		{
			switch (action)
			{
			case FW::Actions::Add:
				Ogre::LogManager::getSingletonPtr()->logMessage("File (" + dir + "\\" + filename + ") Added! ");
				break;
			case FW::Actions::Delete:
				Ogre::LogManager::getSingletonPtr()->logMessage("File (" + dir + "\\" + filename + ") Deleted! ");
				break;
			case FW::Actions::Modified:
				Ogre::LogManager::getSingletonPtr()->logMessage("File (" + dir + "\\" + filename + ") Modified! ");

				break;
			default:
				std::cout << "Should never happen!" << std::endl;
			}
		}
	};
}