#pragma once

#include "Ogre.h"
#include "OIS.h"
#include "OgreFrameListener.h"

#include "FileWatcher\FileWatcher.h"
#include "FileWatcher\FileWatcherWin32.h"

namespace FW
{
	/// Processes a file action
	class TextureReloader : public FW::FileWatchListener
	{
	public:
		TextureReloader() {}
		void handleFileAction(FW::WatchID watchid, const String& dir, const String& filename, FW::Action action)
		{
			// Get the texture with this name (if it exists)
			Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(filename);

			// this is actually a bug. Sometimes the event is received before the system is
			// finished writing the change and you will get file contention errors. So, wait
			// for the write to finish.
			Sleep(100);

			// if the texture exists, reload it
			if (!tex.isNull())
				tex->reload();
		}
	};
}