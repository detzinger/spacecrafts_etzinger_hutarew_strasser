 #ifndef __SpacecraftController_h_
 #define __SpacecraftController_h_ 

#include "Spacecraft.h"

class Path;

class SpacecraftController
{
public:
	SpacecraftController(Spacecraft* spacecraft);
	virtual ~SpacecraftController();

	virtual void update(float delta) = NULL;

	Spacecraft* getSpacecraft()
	{
		return mSpacecraft;
	}

	Ogre::Vector3 seek(const Ogre::Vector3& target) const;
	Ogre::Vector3 arrive(const Ogre::Vector3& target) const;
	Ogre::Vector3 pursue(Spacecraft& evader) const;
	Ogre::Vector3 obstacleAvoidance() const;
	Ogre::Vector3 wallAvoidance(const Spacecraft* spacecraft) const;
	Ogre::Vector3 followPath(const Path& path, float& currentParam) const;

	Ogre::Vector3 followPathLua(const Path& path) const
	{
		float temp = 0;
		return followPath(path, temp);
	}


	Spacecraft* mSpacecraft;
};

#endif